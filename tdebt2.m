function y = tdebt2(sg,e)
N=length(sg);
enorm=sg;sgnorm=sg;
y=zeros(1,N);
for i=2:N
    maxsg=max(sg(1:i));
    maxe=max(e(1:i));
    if maxsg==0
        sgnorm(i)=0;
    else
        %sgnorm(i)=sg(i)/maxsg;
        sgnorm(i)=sg(i)-sg(i-1)/sg(i-1);
    end
    if maxe==0
        enorm(i)=0;
    else
        %enorm(i)=e(i)/maxe;
        enorm(i)=e(i)-e(i-1)/e(i-1);
    end
end
y=sgnorm-enorm;
end

