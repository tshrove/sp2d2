function tdvsstar2(project,repo)
%Get the data from the database.
[datastar, datatd] = getFromDatabase(project, repo);

figure
%Get star data from database prenormalized
xx=0:0.01:1;
xstar = datastar.normalized_percentage;
ystar = datastar.cumlative_value;

%Get td data from database prenormalized
xtd = datatd.project_percentage;
ytd = datatd.cumlative_value;

% clean and interpolate the stargazer data.
[xi,yi,i1] = cleanAndInterpolate(xtd,ytd);

% clean and interpolate the td data.
[xistar,yistar,i1star] = cleanAndInterpolate(xstar,ystar);

% normalize yistart data by maximum value.
sgsn = yistar/max(yistar);

% normalize yi data by maximum value.
tdsn = yi/max(yi);

% sP2D2 rate calculated by stargazer and td data.
td = tdebt2(sgsn, tdsn);

%
%plot everything
subplot(2,1,1);
plot(xistar,sgsn,'b',xx,tdsn,'r');
axis([0 1 0 1]);
legend('star gazer', 'defect debt');
title("defect debt vs github stargazer");
subplot(2,1,2);
plot(xx,td); grid;
title("sP2D2 rate");
sgtitle(strcat(project, " - ", repo));
end

