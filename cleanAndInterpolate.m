function [x,y,i] = cleanAndInterpolate(x, y)
% Get unique values from td time.
[x2,i1,i2] = unique(x);
% Get the intervals that we want to set between each points on the x-axis
dx=(x2(length(x2))-x2(1))/100;
% Create the x-axis values.
xi=x2(1):dx:x2(length(x2));
%
% interpolate td values
y=interp1(x2,y(i1),xi);
i = i1;
x = xi;
end

